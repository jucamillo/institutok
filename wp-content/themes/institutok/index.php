<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package institutok
 */

get_header();
?>


<section class="content-archive-blog">
	<div class="container">
		<div class="col-xs-12 titulo">
			<h1>Blog</h1>
			<?php get_search_form(); ?>
		</div>
		<div class="col-xs-12">
			<ul class="categorias">
			    <?php wp_list_categories( array(
			        'orderby'    => 'name',
			        'show_count' => false,
        			'title_li' => '',
        			'hide_empty' => false,
			    ) ); ?> 
			</ul>
		</div>
		<div class="col-xs-12">
			<?php echo do_shortcode('[destaque_blog]'); ?>
		</div>

		<div class="col-xs-12 recentes">
			<h2>Recentes</h2>
			<div class="list-posts">
				
			<?php
			if ( have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;
				wpbeginner_numeric_posts_nav();

			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>
			</div>
		</div>

	</div>
</section>

<?php
get_footer();
