<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package institutok
 */

get_header();
?>
<style type="text/css">
	
				header .container .col-xs-12{
					border-color:transparent;
				}
				header{
					background: transparent;
				}
				#nome{
					background-color: #C03834;

				}
				header.scroll,
				body{
					background-color:  #C03834;
				}
				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline,
				.woocommerce form .form-row input,
				header.scroll .container .col-xs-12{
					border-color: #4A254B;
				}
				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline,
				footer .container,
				h2, h3, h4, h5, h1, body, h6,
				header .container{
					color: #4A254B;
				}
				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline svg *,
				footer svg *,
				header svg *,
				header .branding svg *{
					fill: #4A254B;
				}
				.miolo-site .woocommerce table.shop_table thead th, .woocommerce .miolo-site table.shop_table thead th,
				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline:hover,
				header ul a.btn_header{
					background-color: #4A254B;
					color: #C03834;
				}
				header ul a.btn_header:hover{
					background-color: #C03834;
					color: #4A254B;

				}

				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline:hover svg *{
					fill: #C03834;

				}
				
				body.page-minha-conta.register-on header.scroll,
				body.page-minha-conta.register-on{
					background-color: #C03834;

				}
				

				h6 {
				    line-height: 1.5;
				    margin: 0 0 60px;
				}


				.inputs button:hover{
					color: #C03834 !important;
					background-color: #4A254B !important;
				}
				.inputs input,
				.inputs,
				.inputs button{
					border-color: #4A254B !important;
					color: #4A254B !important;
				}



				.page-finalizar-compra .btn,
				.page-finalizar-compra a.btn,
				.page-finalizar-compra .alert a.btn,
				.page-carrinho a.btn,
				.page-carrinho .alert a.btn{

				  --buttonColor: #4A254B;
				  --hoverColor: #D7D8D8;
				  --buttonShadowColor: rgba(74,37,75,.5);
				}
				.page-finalizar-compra .modal .alert button.close svg *,
				.page-carrinho .modal .alert button.close svg *{
					fill: #4A254B;

				}
				.page-finalizar-compra a.btn b svg *,
				.page-carrinho a.btn b svg *{
					fill: #C03834;
				}

				.page-finalizar-compra .modal .alert,
				.page-carrinho .modal .alert{
					background: #C03834;
				}

				ul.thwmscf-tabs li a.active{
					color:#D7D8D8;
					background: #4A254B;
				}


</style>

<section class="content-archive-blog single-miolo 404page">
	<div class="container">
		<div class="col-xs-12 col-md-6">
			<h1>Erro 404</h1>

			<h6><?php the_field('texto_404', 'option'); ?></h6>
			<a href="<?php echo get_home_url();?>" class="btn-small">
				Ir para Início
			</a>

		</div>
		<div class="col-xs-12 col-md-6 quote">

			<h2>
				<?php the_field('citacao_404', 'option'); ?>
			</h2>
			<h5>
				<?php the_field('autor_404', 'option'); ?>
			</h5>

		</div>
	</div>
</section>



<?php
get_footer();
