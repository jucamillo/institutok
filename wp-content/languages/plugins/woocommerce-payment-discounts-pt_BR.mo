��          t      �                   %   (  P   N     �     �     �  (   �     �  M     F  S     �     �  )   �  r   �     X     x     �  -   �     �  O   �                                           	          
       %s off Discount amount Discount coupon for payments with %s. Enter an amount (e.g. 20.99, or a percentage, e.g. 5%) for each payment gateway. Payment Discounts Payment Methods Payment method Use zero (0) for not applying discounts. WooCommerce works only with %s 3.0 or later, please install or upgrade your installation! PO-Revision-Date: 2017-04-10 04:11:52+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: pt_BR
Project-Id-Version: Plugins - Discounts Per Payment Method on WooCommerce - Stable (latest release)
 %s de desconto Total do desconto Cupom de desconto para pagamentos com %s. Digite um valor (por exemplo valores fixos, como 20,99 ou em porcentagem, como 5%) para cada método de pagamento. Descontos na forma de pagamento Métodos de pagamento Método de pagamento Utilize zero (0) para não aplicar descontos. WooCommerce funciona apenas com %s 3.0 ou superior, instale ou atualize a sua instalação! 