<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package institutok
 */

get_header();
?>

<?php
while ( have_posts() ) :
	the_post(); ?>
<section class="content-archive-blog single-miolo">
	<div class="container">
		<div class="col-xs-12">
			<?php institutok_post_thumbnail(); ?>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<h1><?php the_title(); ?></h1>
			<article>
            <?php if( get_field('subtitulo') ): ?>
            <h2><?php the_field('subtitulo'); ?></h2>
            <?php endif; ?>

            <?php 
			the_content( );
			?>

			</article>
		</div>

		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<aside class="list-posts ">
				<h4>Textos relacionados</h4>
				<?php echo do_shortcode('[my_related_posts]') ?>
			</aside>
		</div>

	</div>
</section>
<section class="lista-produtos-secao">
	
	<div class="container">
		<div class="col-xs-12">
			<h4>
				Cursos relacionados
			</h4>

		<?php
    $post = $wp_query->post;   
    //$ids = the_field( 'cursos_relacionados');  

		echo $ids;
		 echo do_shortcode('[products columns="3" orderby="title" order="ASC" ids="74,82,83"]');            //[product_category per_page="3" columns="1" orderby="menu_order title" order="ASC" category="destaque-mentorias"]
?>

		</div>
	</div>
</section>

<? endwhile; ?>


<?php

get_footer();
