<form action="<?php echo home_url( '/' ); ?>" role="search" method="get" class="search-form">
    <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Faça aqui sua busca" />
    <input type="hidden" name="post_type[]" value="post" />
    <button><i class="fa fa-search" aria-hidden="true"></i></button>
</form>