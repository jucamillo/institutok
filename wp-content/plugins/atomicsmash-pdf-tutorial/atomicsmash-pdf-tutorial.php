<?php
/*
* Plugin Name: Atomic Smash FPDF Tutorial
* Description: A plugin created to demonstrate how to build a PDF document from WordPress posts.
* Version: 1.0
* Author: Anthony Hartnell
* Author URI: https://www.atomicsmash.co.uk/blog/author/anthony/
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

include( 'atomicsmash-pdf-helper-functions.php');
$pdf = new PDF_HTML();

if( isset($_POST['generate_posts_pdf'])){
    output_pdf();
}

add_action( 'admin_menu', 'as_fpdf_create_admin_menu' );
function as_fpdf_create_admin_menu() {
    $hook = add_submenu_page(
        'tools.php',
        'Atomic Smash PDF Generator',
        'Atomic Smash PDF Generator',
        'manage_options',
        'as-fdpf-tutorial',
        'as_fpdf_create_admin_page'
    );
}

function output_pdf() {
    $posts = get_posts( 'posts_per_page=5' );

    if( ! empty( $posts ) ) {
        global $pdf;
        $title_line_height = 10;
        $content_line_height = 8;

        $pdf->AddPage();
        $pdf->SetFont( 'Arial', '', 42 );
        $pdf->Write(20, 'Atomic Smash FPDF Tutorial');

        foreach( $posts as $post ) {

            $pdf->AddPage();
            $pdf->SetFont( 'Arial', '', 22 );
            $pdf->Write($title_line_height, $post->post_title);

            // Add a line break
            $pdf->Ln(15);

            // Image
            $page_width = $pdf->GetPageWidth() - 20;
            $max_image_width = $page_width;

            $image = get_the_post_thumbnail_url( $post->ID );
            if( ! empty( $image ) ) {
                $pdf->Image( $image, null, null, 100 );
            }
            
            // Post Content
            $pdf->Ln(10);
            $pdf->SetFont( 'Arial', '', 12 );
            $pdf->WriteHTML($post->post_content);
        }
    }

    $pdf->Output('D','atomic_smash_fpdf_tutorial.pdf');
    exit;
}


function as_fpdf_create_admin_page() {
?>
<div class="wrap">
    <h1>Atomic Smash - Wordpress PDF Generator</h1>
    <p>Click below to generate a pdf from the content inside all the Wordpress Posts. </p>
    <p>Each post will be on its own pdf page containing the post title and post content.</p>
	<form method="post" id="as-fdpf-form">
        <button class="button button-primary" type="submit" name="generate_posts_pdf" value="generate">Generate PDF from Wordpress Posts</button>
    </form>
</div>
<?php
}