<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package institutok
 */

get_header();
?>


<section class="content-archive-blog">
	<div class="container">
		<div class="col-xs-12 titulo">
			<h2>

					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Resultado de busca por: %s', 'institutok' ), '<span>' . get_search_query() . '</span>' );
					?>
			</h2>
			<?php get_search_form(); ?>
		</div>
		<div class="col-xs-12">
			<ul class="categorias">
			    <?php wp_list_categories( array(
			        'orderby'    => 'name',
			        'show_count' => false,
        			'title_li' => '',
        			'hide_empty' => false,
			    ) ); ?> 
			</ul>
		</div>

		<div class="col-xs-12 recentes">
			<div class="list-posts">
				
			<?php
			if ( have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;
				wpbeginner_numeric_posts_nav();

			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>
			</div>
		</div>

	</div>
</section>


<?php
get_footer();
