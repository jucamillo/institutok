<?php
/**
 * institutok functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package institutok
 */

if ( ! function_exists( 'institutok_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function institutok_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on institutok, use a find and replace
		 * to change 'institutok' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'institutok', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Principal', 'institutok' ),
            'menu-2' => esc_html__( 'Rodapé', 'institutok' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'institutok_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'institutok_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function institutok_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'institutok_content_width', 640 );
}
add_action( 'after_setup_theme', 'institutok_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function institutok_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'institutok' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'institutok' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'institutok_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function institutok_scripts() {
	wp_enqueue_style( 'institutok-style', get_stylesheet_uri() );

	wp_enqueue_script( 'institutok-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'institutok-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'institutok_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}









///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////ADAPTAÇOES JUCAMILLO

//CLASSE PARENT E CLASSE NOME NO BODY
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );
add_filter('body_class','body_class_section');
function body_class_section($classes) {
    global $wpdb, $post;
    if (is_page()) {
        if ($post->post_parent) {
            $parent  = end(get_post_ancestors($current_page_id));
        } else {
            $parent = $post->ID;
        }
        $post_data = get_post($parent, ARRAY_A);
        $classes[] = 'parent-' . $post_data['post_name'];
    }
    return $classes;
}



//BREADCRUMBS

function custom_breadcrumbs() {
       
    // Settings
    $separator          = '/';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'breadcrumbs';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = 'product_cat';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li class="separator separator-home"> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
                echo $cat_display;
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}
add_shortcode( 'custom_breadcrumbs', 'custom_breadcrumbs' );


//PAGINAÇÃO

function wpbeginner_numeric_posts_nav() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="navigation"><ul>' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );
 
    echo '</ul></div>' . "\n";
 
}



//FUNCAO IF IS POST TYPE
function is_post_type($type){
    global $wp_query;
    if($type == get_post_type($wp_query->post->ID)) return true;
    return false;
}




//REMOVER 'TITULOS' DOS ARCHIVES

add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});


//CRIAR OPTIONS MENU ACF PRO
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Opções do Tema');
}



//PAGINACAO CUSTOM
function custom_pagination($numpages = '', $pagerange = '', $paged='') {if (empty($pagerange)) {$pagerange = 2;}/**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */global $paged;if (empty($paged)) {$paged = 1;}if ($numpages == '') {global $wp_query;$numpages = $wp_query->max_num_pages;if(!$numpages) {$numpages = 1;}}/** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */$pagination_args = array(
            'base'=> get_pagenum_link(1) . '%_%',
            'format'=> 'page/%#%',
            'total'=> $numpages,
            'current'=> $paged,
            'show_all'=> False,
            'end_size'=> 1,
            'mid_size'=> $pagerange,
            'prev_next'=> True,
            'prev_text'=> __('«'),
            'next_text'=> __('»'),
            'type'=> 'plain',
            'add_args'=> false,
            'add_fragment'=> '');

   $paginate_links = paginate_links($pagination_args);
   if ($paginate_links) {echo "<div class='full-pag'><nav class='custom-pagination'>";echo $paginate_links;echo "</nav></div>";}}








//SHORTCODE POSTS RELACIONADOS
function my_related_posts() {
     $args = array('posts_per_page' => 3,  'post_in'  => get_the_tag_list(), 'post_not_in' => get_the_id() );
     $the_query = new WP_Query( $args );


     while ( $the_query->have_posts() ) : $the_query->the_post();

    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
    echo '<div class="post-latest"><a href="'.get_the_permalink().'" class="img" style="background-image:url('.$image[0].');">
        </a>
        <h4>
            <a href="'.get_the_permalink().'" title="'.get_the_title().'">
            '.get_the_title().'
            </a>
        </h4>

        </div>
        '; 

      endwhile;


      wp_reset_postdata();
}
add_shortcode( 'my_related_posts', 'my_related_posts' );




//REDIRECIONAMENTO DE SINGLE POSTS LISTAGEM
add_action( 'template_redirect', 'subscription_redirect_post' );

function subscription_redirect_post() {
  $queried_post_type = get_query_var('post_type');
  /*if ( is_single() && 'boleto' ==  $queried_post_type ) {
    wp_redirect( home_url( 'boletos', 'relative' ), 301 );
    exit;
  }*/
  if ( is_single() && 'banner' ==  $queried_post_type ) {
    wp_redirect( home_url(), 301 );
    exit;
  }
}




function botao( $atts ){
    
    // set up default parameters
    extract(shortcode_atts(array(
     'link' => '#',
     'target' => '_self',
     'texto' => 'Saiba Mais',
     'class' => ''
    ), $atts));

    $string = '';

    $category = get_term_by( 'name', $categoria, 'product_cat' );

    $string .= '<a class="btn '.$class.'" href="'.$link.'" target="'.$target.'" title="'.$texto.'"> 
                 '.$texto.'
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <b>'.$texto.'</b>
                  <b>'.$texto.'</b>
                  <b>'.$texto.'</b>
                  <b>'.$texto.'</b>
                </a>';

        return $string;
}
add_shortcode( 'botao', 'botao' );




function mytheme_add_woocommerce_support() {

    add_theme_support( 'woocommerce' );

}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );



add_filter('woocommerce_style_smallscreen_breakpoint','woo_custom_breakpoint');



function woo_custom_breakpoint($px) {

  $px = '767px';

  return $px;

}

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );

remove_action('woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title',10);






add_action( 'woocommerce_register_form_start', 'bbloomer_add_name_woo_account_registration' );
  
function bbloomer_add_name_woo_account_registration() {
    ?>
  
    <p class="form-row form-row-first">
    <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
    </p>
  
    <p class="form-row form-row-last">
    <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?> <span class="required">*</span></label>
    <input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
    </p>
  
    <div class="clear"></div>
  
    <?php
}
  
///////////////////////////////
// 2. VALIDATE FIELDS
  
add_filter( 'woocommerce_registration_errors', 'bbloomer_validate_name_fields', 10, 3 );
  
function bbloomer_validate_name_fields( $errors, $username, $email ) {
    if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
        $errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
    }
    if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
        $errors->add( 'billing_last_name_error', __( '<strong>Error</strong>: Last name is required!.', 'woocommerce' ) );
    }
    return $errors;
}
  
///////////////////////////////
// 3. SAVE FIELDS
  
add_action( 'woocommerce_created_customer', 'bbloomer_save_name_fields' );
  
function bbloomer_save_name_fields( $customer_id ) {
    if ( isset( $_POST['billing_first_name'] ) ) {
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
        update_user_meta( $customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']) );
    }
    if ( isset( $_POST['billing_last_name'] ) ) {
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
        update_user_meta( $customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']) );
    }
  }
add_filter('woocommerce_sale_flash', 'lw_hide_sale_flash');
function lw_hide_sale_flash()
{
return false;
}

add_action( 'woocommerce_after_shop_loop_item_title', 'show_sale_percentage_loop', 25 );
function show_sale_percentage_loop() {
    global $product;
    if ( ! $product->is_on_sale() ) return;
    if ( $product->is_type( 'simple' ) ) {
        $max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;
    } elseif ( $product->is_type( 'variable' ) ) {
        $max_percentage = 0;
        foreach ( $product->get_children() as $child_id ) {
            $variation = wc_get_product( $child_id );
            $price = $variation->get_regular_price();
            $sale = $variation->get_sale_price();
            if ( $price != 0 && ! empty( $sale ) ) $percentage = ( $price - $sale ) / $price * 100;
            if ( $percentage > $max_percentage ) {
                $max_percentage = $percentage;
            }
        }
    }
    if ( $max_percentage > 0 ) echo "<div class='sale_perc'>" . round($max_percentage) . "% OFF</div>";
}
add_shortcode( 'show_sale_percentage_loop', 'show_sale_percentage_loop' );

add_action( 'woocommerce_single_product_summary', 'show_Psale_percentage_loop', 10 );
function show_Psale_percentage_loop() {
    global $product;
    if ( ! $product->is_on_sale() ) return;
    if ( $product->is_type( 'simple' ) ) {
        $max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;
    } elseif ( $product->is_type( 'variable' ) ) {
        $max_percentage = 0;
        foreach ( $product->get_children() as $child_id ) {
            $variation = wc_get_product( $child_id );
            $price = $variation->get_regular_price();
            $sale = $variation->get_sale_price();
            if ( $price != 0 && ! empty( $sale ) ) $percentage = ( $price - $sale ) / $price * 100;
            if ( $percentage > $max_percentage ) {
                $max_percentage = $percentage;
            }
        }
    }
    if ( $max_percentage > 0 ) echo "<div class='sale_perc'>" . round($max_percentage) . "% OFF</div>";
}


// Add specific CSS class by filter
add_filter('body_class','er_logged_in_filter');
function er_logged_in_filter($classes) {
if( is_user_logged_in() ) {
$classes[] = 'loggedin';
} else {
$classes[] = 'loggedout';
}
// return the $classes array
return $classes;
}







/*
add_action( 'save_post_product', 'update_product_set_sale_cat' );
function update_product_set_sale_cat( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }

    if ( ! current_user_can( 'edit_product', $post_id ) ) {
        return $post_id;
    }

    if( get_post_status( $post_id ) == 'publish' && isset($_POST['_sale_price']) ) {
        $sale_price = $_POST['_sale_price'];

        if( $sale_price >= 0 && ! has_term( 'Ofertas', 'tipo', $post_id ) ){
            wp_set_object_terms($post_id, 'Ofertas', 'tipo', true );
        } else{

            wp_remove_object_terms($post_id, 'Ofertas', 'tipo', true );
        }
    }
}

*/



function galeria_produto( $atts ){
    
    global $product;

    $string = '';
    $cor = get_field('cor');


    $cor2 = str_replace('#', '', $cor);
    $attachment_ids = $product->get_gallery_image_ids();

        $string .='<div class="gallery">
        <button class="prev"><i class="fas fa-chevron-left"></i></button>
        <button class="next"><i class="fas fa-chevron-right"></i></button>
  <div class="images">';
    foreach( $attachment_ids as $attachment_id ) {
        $string .='<a data-fancybox="gallery" href="'.wp_get_attachment_url( $attachment_id ).'" class="image" style="background-image:url('.wp_get_attachment_url( $attachment_id ).');">
                  </a>';
        }
    $string .='</div>
  <div class="thumbs">';
    foreach( $attachment_ids as $attachment_id ) {
        $string .='<div class="thumb" style="background-image: linear-gradient(0deg, #'.$cor2.'B3, #'.$cor2.'B3), linear-gradient(0deg, #FFFFFF, #FFFFFF), url('.wp_get_attachment_url( $attachment_id ).');"></div>';
        }
$string .='</div>';
$string .='</div>';



        return $string;
}
add_shortcode( 'galeria_produto', 'galeria_produto' );





function depoimento_produto( $atts ){
    
    $string = '';
    $cor = get_field('cor');


    $cor2 = str_replace('#', '', $cor);
    if ( have_rows('depoimentos') ):
    $string .='<div class="depoimentos">';
        while ( have_rows('depoimentos') ) : the_row(); 
        $string .='<div class="item"> 
                        <blockquote>'.get_sub_field('depoimento').'</blockquote>
                        <div class="pessoa">
                            <div class="img" style="background: linear-gradient(0deg, #'.$cor2.'B3, #'.$cor2.'B3), linear-gradient(0deg, #FFFFFF, #FFFFFF), url('.get_sub_field('foto').')"></div>
                            <p> <strong>'.get_sub_field('nome').'</strong><br>
                                '.get_sub_field('posicao').'
                            </p>
                        </div>
                    </div>';
        endwhile;
        $string .='</div>';
    endif; 

        return $string;
}
add_shortcode( 'depoimento_produto', 'depoimento_produto' );


add_filter( 'wc_add_to_cart_message_html', '__return_false' );

function info_produto( $atts ){
    
    global $product;

    $numparcelas = get_field('parcelas');

    $parcelas = $product->get_price() / $numparcelas;
    $avista = $product->get_price() * .9;

    


        if ( $product->is_type( 'variable' ) ) :

            $string = '
                <div class="precos">
                    <div class="linha">
                        <div>';
                            if( get_field('professor') ):    
                    $string .= '<h3>
                                    Quem vai ensinar?
                                </h3>
                                <h5>'.get_field('professor').'</h5>';
                            endif;
                    $string .= '</div>
                        <div>';   
                            if( get_field('horas') ):
                    $string .= '<h3>Carga horária</h3>
                                <h5>'.get_field('horas').'</h5>';
                            endif;
                        $string .= '</div>
                    </div> ';
            
            if( get_field('venda') ):
                $string .= '<div class="linha">
                                <div>';
                                    if( get_field('cidade') ):
                                        $string .= '<h6>
                                            '.get_field('cidade').'
                                        </h6>';
                                    endif; 
                                    if( get_field('datas_e_horarios') ): 
                                    $string .= '<p>
                                            '.get_field('datas_e_horarios').'
                                        </p>';
                                    endif;
                                $string .= '</div>
                                <div><a href="'.$product->get_title().'" class="btn-small interesse" title="Tenho interesse">
                                       Tenho interesse
                                    </a></div></div>';

            else:


                foreach ($product->get_children() as $child_id) {
                    $_product = wc_get_product( $product_id );
                    $variation = wc_get_product($child_id);

                    $var = $variation->get_variation_description();

                    $desc = str_replace('|', '<br>', $var);

                      

                    $cid = $variation->get_attribute('cidade');

                    $string .= '<div class="linha variable_prod" id="'.$child_id.'"> 
                                <div> 
                                    <h6>'.$cid.'</h6>
                                    <p>'.$desc.'</p> 
                                </div>
                                <div>
                                    <h6>'.$numparcelas.'x R$'. number_format($parcelas, 2, ',', '.').'</h6>
                                    <p>
                                        ou R$'. number_format($avista, 2, ',', '.').' à vista<br>
                                        (10% de desconto)
                                    </p>
                                    <a href="?add-to-cart='.$child_id.'" data-quantity="1" class="btn-small add-carrinho" title="Adicionar ao carrinho">
                                        Adicionar ao carrinho
                                    </a>
                                    <div class="qtd">
                                        <div class="inputs" cidade=" - '.$cid.'">
                                            <button class="down">-</button>
                                            <input type="number">
                                            <button class="up">+</button>
                                            <div class="alert_numeros maisum">
                                                1 adicionado <br>
                                                ao carrinho
                                            </div>
                                            <div class="alert_numeros menosum">
                                                1 removido <br>
                                                do carrinho
                                            </div>
                                        </div>
                                        <a href="'.get_home_url().'/carrinho" tite="Ir para o carrinho"  class="cart">
                                            Ir para o<br> carrinho
                                        </a>
                                    </div>
                                </div>
                            </div>';

                 }
            endif;
        else:



            if( get_field('venda') ):
                $string = '<div class="precos">
                    <div class="linha">
                        <div>';
                            if( get_field('professor') ):    
                            $string .= '<h3>
                                    Quem vai ensinar?
                                </h3>
                                <h5>'.get_field('professor').'</h5>';
                            endif;
                        $string .= '</div><div>';   
                            if( get_field('horas') ):
                    $string .= '<h3>Carga horária</h3>
                                <h5>'.get_field('horas').'</h5>';
                            endif;
                        $string .= '</div>

                    </div>';
                $string .= '<div class="linha">
                                <div>';
                                    if( get_field('cidade') ):
                                        $string .= '<h6>
                                            '.get_field('cidade').'
                                        </h6>';
                                    endif; 
                                    if( get_field('datas_e_horarios') ): 
                                    $string .= '<p>
                                            '.get_field('datas_e_horarios').'
                                        </p>';
                                    endif;
                                $string .= '</div>
                                <div><a href="'.$product->get_title().'" class="btn-small interesse" title="Tenho interesse">
                                       Tenho interesse
                                    </a></div></div>';

            else:


            $string = '<div class="precos">
                    <div class="linha">
                        <div>';
                            if( get_field('professor') ):    
                            $string .= '<h3>
                                    Quem vai ensinar?
                                </h3>
                                <h5>'.get_field('professor').'</h5>';
                            endif;
                        $string .= '</div><div>';   
                            if( get_field('horas') ):
                    $string .= '<h3>Carga horária</h3>
                                <h5>'.get_field('horas').'</h5>';
                            endif;
                        $string .= '</div>

                    </div><div class="linha">
            <div>';
                if( get_field('cidade') ):
                    $string .= '<h6>
                        '.get_field('cidade').'
                    </h6>';
                endif; 
                if( get_field('datas_e_horarios') ): 
                $string .= '<p>
                        '.get_field('datas_e_horarios').'
                    </p>';
                endif;

            $string .= '</div>';


            $string .= '<div>
                <h6>'.$numparcelas.'x R$'. number_format($parcelas, 2, ',', '.').'</h6>
                <p>
                    ou R$'. number_format($avista, 2, ',', '.').' à vista<br>
                    (10% de desconto)
                </p>';





                $string .= '<a href="?add-to-cart='.$product->get_id().'" data-quantity="1" class="btn-small add-carrinho" title="Adicionar ao carrinho">
                    Adicionar ao carrinho
                </a>

                <div class="qtd">
                    <div class="inputs">
                        <button class="down">-</button>
                        <input type="number">
                        <button class="up">+</button>
                        <div class="alert_numeros maisum">
                            1 adicionado <br>
                            ao carrinho
                        </div>
                        <div class="alert_numeros menosum">
                            1 removido <br>
                            do carrinho
                        </div>


                    </div>
                    <a href="'.get_home_url().'/carrinho" tite="Ir para o carrinho"  class="cart">
                        Ir para o<br> carrinho
                    </a>
                </div>
            </div>';


        $string .= '</div>';
    endif;

    endif;

        
    
        $string .= '</div>';

        return $string;
}
add_shortcode( 'info_produto', 'info_produto' );




function cta_Whats( $atts ){
    
    // set up default parameters
    extract(shortcode_atts(array(
     'link' => 'https://api.whatsapp.com/send/?phone=5583998146152',
     'chamada' => 'Se ainda tiver alguma dúvida, fale comigo pelo WhatsApp:',
     'num' => '83 98765 4321',
     'class' => ''
    ), $atts));
    $string = '
    <div class="cta-whats">
        <h4>'.$chamada.'</h4>
        <a href="'.$link.'" class="btn" target="_blank">

                 '.$num.' <img class="icon" src="'.get_template_directory_uri().'/images/whats.svg">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <b>'.$num.' <img class="icon" src="'.get_template_directory_uri().'/images/whats.svg"></b>
                  <b>'.$num.' <img class="icon" src="'.get_template_directory_uri().'/images/whats.svg"></b>
                  <b>'.$num.' <img class="icon" src="'.get_template_directory_uri().'/images/whats.svg"></b>
                  <b>'.$num.' <img class="icon" src="'.get_template_directory_uri().'/images/whats.svg"></b>

        </a>
        </div>';

        return $string;
}
add_shortcode( 'cta_Whats', 'cta_Whats' );





function destaque_blog( $atts ) {
        // set up default parameters
        extract(shortcode_atts(array(
         'categoria' => 'Blog'
        ), $atts));

        $args = array(
            'post_type' => 'post',
            'orderby'=>'rand',
            'posts_per_page' => 3,
            'tax_query' => array(
                array(
                    'taxonomy' => 'destaque',
                    'field' => 'name',
                    'terms' => $categoria,
                ),
            ),
        );
        $string = '';
        $query = new WP_Query( $args );
        if( $query->have_posts() ){           
            $string .= '<h2>Destaques</h2>
            <ul class="destaque-conteudo">';
            while( $query->have_posts() ){
                $query->the_post();
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                $string .= '<li>
                                <ul class="blog-categories">';

                                $categories = wp_get_post_categories( get_the_ID() );
                                    //loop through them
                                    foreach($categories as $c){
                                      $cat = get_category( $c );
                                      //get the name of the category
                                      $cat_id = get_cat_ID( $cat->name );
                                      //make a list item containing a link to the category
                                       $string .= '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></li>';
                                    }
                                $string .= '
                                </ul> 
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');">

                                </a>
                                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="info">
                                    <h4>'.get_the_title().'</h4>
                                    <p>'.strip_tags( get_the_excerpt() ).'</p>
                                </a>                 
                            </li>';
            }

            $string .= '</ul>';

        }

        wp_reset_postdata();
        return $string;
}

add_shortcode('destaque_blog', 'destaque_blog');

function mytheme_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'mytheme_custom_excerpt_length', 999 );


function custom_length_excerpt($word_count_limit) {
    $content = wp_strip_all_tags(get_the_content() , true );
    echo wp_trim_words($content, $word_count_limit);
}



function iconic_remove_password_strength() {
    wp_dequeue_script( 'wc-password-strength-meter' );
}
add_action( 'wp_print_scripts', 'iconic_remove_password_strength', 10 );


  
add_filter( 'body_class', 'bbloomer_wc_product_cats_css_body_class' );
  
function bbloomer_wc_product_cats_css_body_class( $classes ){
  if ( is_singular( 'product' ) ) {
    $current_product = wc_get_product();
    $custom_terms = get_the_terms( $current_product->get_id(), 'product_cat' );
    if ( $custom_terms ) {
      foreach ( $custom_terms as $custom_term ) {
        $classes[] = 'product_cat_' . $custom_term->slug;
      }
    }
  }
  return $classes;
}

add_filter( 'woocommerce_account_menu_items', 'misha_remove_my_account_dashboard' );
function misha_remove_my_account_dashboard( $menu_links ){
 
    unset( $menu_links['dashboard'] );
    return $menu_links;
 
}

add_action('template_redirect', 'misha_redirect_to_orders_from_dashboard' );
 
function misha_redirect_to_orders_from_dashboard(){
 
    if( is_account_page() && empty( WC()->query->get_current_endpoint() ) ){
        wp_safe_redirect( wc_get_account_endpoint_url( 'orders' ) );
        exit;
    }
 
}


add_filter ( 'woocommerce_account_menu_items', 'misha_rename_downloads' );
 
function misha_rename_downloads( $menu_links ){
 
    // $menu_links['TAB ID HERE'] = 'NEW TAB NAME HERE';
    $menu_links['downloads'] = 'Materiais';
    $menu_links['orders'] = 'Cursos';
    $menu_links['edit-address'] = 'Dados da nota';
    $menu_links['edit-account'] = 'Dados da conta';
 
    return $menu_links;
}






function foto_prof( $atts ){
    
    $string = '
    <div class="foto_prof" style="background: linear-gradient(0deg, '.get_field('bg').', '.get_field('bg').'), linear-gradient(0deg, #FFFFFF, #FFFFFF), url('.get_field('foto_prof').')">
    </div>';

        return $string;
}
add_shortcode( 'foto_prof', 'foto_prof' );






function video_prod( $atts ){
    
    $string = '
    <div class="video_curso">
        <button> </button>
        '.get_field('video').'
    </div>';

        return $string;
}
add_shortcode( 'video_prod', 'video_prod' );
