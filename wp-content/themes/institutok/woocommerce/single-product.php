<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.4/hammer.min.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/gallery.css">
<script src="<?php echo get_template_directory_uri(); ?>/js/gallery.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		//do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : ?>
			<?php the_post(); 
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			
		    $cor = get_field('cor');


		    $cor2 = str_replace('#', '', $cor);

			?>

				<style type="text/css">
				header .container .col-xs-12{
					border-color:transparent;
				}
				header{
					background: transparent;
				}

				
				.depoimentos .item blockquote:before,
				.miolo-single-produto .wpb_single_image:before,
				.modal .alert.follow,
				header.scroll,
				body{
					background-color:  <?php echo get_field('bg'); ?>;
				}
				.woocommerce form .form-row input,
				header.scroll .container .col-xs-12{
					border-color: #D7D8D8;
				}
				.modal .alert.follow,
				footer .container,
				h2, h3, h4, h5, h1, body,
				header .container{
					color: #D7D8D8;
				}

				.modal .alert.follow button svg *,
				footer svg *,
				header svg *,
				header .branding svg *{
					fill: #D7D8D8;
				}

				.gallery > button,
				.miolo-single-produto .precos .btn-small:hover,
				header ul a.btn_header{
					background-color: #D7D8D8;
					color: <?php echo get_field('bg'); ?>;
				}
				a.btn,
				.modal .alert.follow a.btn{
				  --hoverColor: <?php echo get_field('bg'); ?>;
				}
				a.btn b svg *{
					fill: <?php echo get_field('bg'); ?>;
				}
				.vc_separator .vc_sep_line{
					border-color: <?php echo get_field('cor'); ?> !important;
				}
				.single-product .yith-wcwl-add-to-wishlist a:hover,
				 .btn-small:hover,
				.cta-whats,
				.miolo-single-produto .precos{
					background-color:  <?php echo get_field('cor'); ?> !important;
				}

				.miolo-single-produto .precos .qtd .inputs button:hover{
					color: <?php echo get_field('cor'); ?> !important;
				}

				.video_curso button,
				.single-product .wpb_video_widget:after{
					background: linear-gradient(0deg, #<?php echo $cor2; ?>B3, #<?php echo $cor2; ?>B3), linear-gradient(0deg, #FFFFFF, #FFFFFF), url(<?php echo $image[0]  ?>);
					background-size: cover;
					background-position: center;
				background-blend-mode: normal, color, normal;
				}
				</style>
				<?php
				if( has_term( 'mentorias', 'product_cat' ) ) { ?>

					<section class="miolo-single-produto">
						<div class="container">
							<div class="col-lg-8 col-md-9 col-sm-12 col-xs-12">
									<?php
									the_content();




									?>
							</div>
							<?php if( get_field('icone') ): ?>
							<div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
								
									<img src="<?php the_field('icone'); ?>" class="icon">
							</div>
							<?php endif; ?>
						</div>
						
					</section>

					<?php
				} else { ?>

					<section class="miolo-single-produto">
						<div class="container">
							<div class="col-lg-8 col-md-9 col-sm-12 col-xs-12">
								<div class="wrap-ctas">
									<a href="#comprar" class="btn-small scroll_to_buy" title="Quero este curso">
										<span>QUERO ESTE CURSO</span>
										<img src="<?php echo get_template_directory_uri(); ?>/images/cart.svg">
									</a>

									<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
								</div>
									<?php
									the_content();




									?>
							</div>
							<?php if( get_field('icone') ): ?>
							<div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
								
									<img src="<?php the_field('icone'); ?>" class="icon">
							</div>
							<?php endif; ?>
						</div>
						
					</section>
					<?php }

				if( get_field('venda') ):

				?>
				<section class="miolo-single-produto" id="tenho_interesse">
					<div class="container">
						<div class="col-xs-12 col-sm-12 col-md-2">
							
						</div>
						<div class="col-xs-12 col-md-8 col-sm-12">
							<div class="nome1">
								
							<?php if( has_term( 'mentorias', 'product_cat' )) : ?>
								<h4>Mentoria</h4>
							<?php else: ?>
								<h4>Curso</h4>
							<?php endif; ?>

							<h5><?php echo get_the_title(); ?></h5>
							</div>
							<?php echo do_shortcode('[contact-form-7 id="281" title="Tenho Interesse"]');?>
						</div>
					</div>
					
				</section>

				



			<?php endif;


			//wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		//do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>
<script type="text/javascript"> 
<?php global $product; if ( $product->is_type( 'variable' ) ) : ?>


	jQuery('.yith-wcwl-add-to-wishlist a').addClass('btn-small');

	jQuery('.gallery .image:first-child').addClass('active');
	jQuery('.gallery .thumb:first-child').addClass('active');

	jQuery(function () {

	  jQuery('.gallery').gallery();


	    jQuery( ".inputs" ).each(function( ) {
    		var cidade = jQuery(this).attr('cidade');

		    var qtd = jQuery('.woofc-item[data-name="<?php echo get_the_title();?>' + cidade + '"] input.qty').val();
		    jQuery(this).find('input').attr('value', qtd);
		});

	});


jQuery(document).delegate('.qtd .inputs button.up', 'click', function(event) {
    event.preventDefault();

    var cidade = jQuery(this).parent('.inputs').attr('cidade');
    console.log(cidade);



    jQuery('.woofc-item[data-name="<?php echo get_the_title();?>' + cidade + '"] .woofc-item-qty-plus').trigger( "click" );
    var qtd = jQuery('.woofc-item[data-name="<?php echo get_the_title();?>' + cidade + '"] input.qty').val();
    jQuery(this).parent().find('input').attr('value', qtd);

    jQuery(this).parent().find('.alert_numeros.maisum').addClass('act');



	setTimeout(function(){
    	jQuery('.inputs .alert_numeros.maisum').removeClass('act');
	}, 1200);


});



jQuery(document).delegate('.qtd .inputs button.down', 'click', function(event) {
    event.preventDefault();
    jQuery('.woofc-item[data-name="<?php echo get_the_title();?>' + cidade + '"] .woofc-item-qty-minus').trigger( "click" );
    var qtd = jQuery('.woofc-item[data-name="<?php echo get_the_title();?>' + cidade + '"] input.qty').val();
    jQuery(this).parent().find('input').attr('value', qtd);


    jQuery(this).parent().find('.alert_numeros.menosum').addClass('act');



	setTimeout(function(){
    	jQuery('.inputs .alert_numeros.menosum').removeClass('act');
	}, 1200);
});



<?php else: ?>



	jQuery('.yith-wcwl-add-to-wishlist a').addClass('btn-small');

	jQuery('.gallery .image:first-child').addClass('active');
	jQuery('.gallery .thumb:first-child').addClass('active');

	jQuery(function () {

	  jQuery('.gallery').gallery();


	    jQuery( ".inputs" ).each(function( ) {
		    var qtd = jQuery('.woofc-item[data-name="<?php echo get_the_title();?>"] input.qty').val();
		    jQuery(this).find('input').attr('value', qtd);
		});

	});


jQuery(document).delegate('.qtd .inputs button.up', 'click', function(event) {
    event.preventDefault();



    jQuery('.woofc-item[data-name="<?php echo get_the_title();?>"] .woofc-item-qty-plus').trigger( "click" );
    var qtd = jQuery('.woofc-item[data-name="<?php echo get_the_title();?>"] input.qty').val();
    jQuery(this).parent().find('input').attr('value', qtd);

    jQuery(this).parent().find('.alert_numeros.maisum').addClass('act');


	setTimeout(function(){
    	jQuery('.inputs .alert_numeros.maisum').removeClass('act');
	}, 1200);


});



jQuery(document).delegate('.qtd .inputs button.down', 'click', function(event) {
    event.preventDefault();
    jQuery('.woofc-item[data-name="<?php echo get_the_title();?>"] .woofc-item-qty-minus').trigger( "click" );
    var qtd = jQuery('.woofc-item[data-name="<?php echo get_the_title();?>"] input.qty').val();
    jQuery(this).parent().find('input').attr('value', qtd);


    jQuery(this).parent().find('.alert_numeros.menosum').addClass('act');



	setTimeout(function(){
    	jQuery('.inputs .alert_numeros.menosum').removeClass('act');
	}, 1200);
});

<?php endif; ?>



jQuery(function(){



    var link = window.location.href;
    //alert(link);
    var values = link.split('-to-cart');
    var check = values[0];
    var check2 = check.split('/?');
    var checkid = link.split('?add-to-cart=');

	if(check2[1] == 'add') {

		jQuery('.miolo-single-produto .precos').addClass('oncart');
			
		jQuery('.miolo-single-produto .precos.oncart .variable_prod#' + checkid[1]).addClass('onvar');




	    var divPosition = jQuery('#comprar').offset();
	    var scrollPosition = divPosition.top - 100;
	    jQuery('html, body').animate({scrollTop: scrollPosition}, 400);

	} else {
		jQuery('.miolo-single-produto .precos').removeClass('oncart');
		
	}



   jQuery('input[name="telefone"]').mask("(99) 9999-9999?9");
});

jQuery(document).delegate('.scroll_to_buy', 'click', function(event) {
    event.preventDefault();



    var divPosition = jQuery('#comprar').offset();
    var scrollPosition = divPosition.top - 100;
    jQuery('html, body').animate({scrollTop: scrollPosition}, 400);


});


jQuery(document).delegate('.gallery > button.next', 'click', function(event) {
    event.preventDefault();
    jQuery( ".gallery .thumbs .thumb.active + *" ).trigger( "click" );
});

jQuery(document).delegate('.gallery > button.prev', 'click', function(event) {
    event.preventDefault();
    jQuery( ".gallery .thumbs .thumb.active" ).prev().trigger( "click" );
});


jQuery(document).delegate('.video_curso button', 'click', function(event) {
    event.preventDefault();
    jQuery(this).addClass('tocar');
    console.log('a');


    var link = jQuery(".video_curso iframe").attr('src');
    var values = link.split('?feature=oembed');
    var url = values[0];


    jQuery(".video_curso iframe").attr('src', url + '?autoplay=1'); 

});



jQuery(document).delegate('a.btn-small.interesse', 'click', function(event) {
    event.preventDefault();
    var id = jQuery(this).attr('href');
    jQuery('input[name="id_produto"]').attr('value', id);
    jQuery( ".nome1" ).insertBefore( jQuery( "form.wpcf7-form .woocommerce" ) );

    jQuery('body').addClass('tenho_interesse');

});






</script>
<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
