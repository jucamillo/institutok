<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>

<?php // echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>

<?php if ( !is_page('seguindo') ) : ?>
<a href="<?php echo $product->get_permalink(); ?>" title="<?php the_title(); ?>" style="background-color: <?php the_field('bg'); ?>;" class="link-curso" id="<?php echo $product->get_id(); ?>">
	<?php if( get_field('icone') ): ?>
		<img src="<?php the_field('icone'); ?>" class="icon">
	<?php endif; ?>

	<div class="titulo">
		<h3><?php the_title(); ?></h3>
		<?php if( get_field('subtitulo') ): ?>
			<h5><?php the_field('subtitulo'); ?></h5>	
		<?php endif; ?>	
	</div>
	<div class="info">
		<?php if( get_field('horas') ): ?>
			<h5><?php the_field('horas'); ?></h5>
		<?php endif; ?>
		<?php if( get_field('professor') ): ?>
			<h5><?php the_field('professor'); ?></h5>
		<?php endif; ?>
	</div>
	
</a>
<?php endif;?>