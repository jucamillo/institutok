<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package institutok
 */

?>
	</section>

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="col-xs-12">
                <div class="info">
                    <?php if( get_field('copyrights', 'option') ): ?>
                     <p><?php the_field('copyrights', 'option'); ?></p>
                    <?php endif; ?>

                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-2',
                    ) );
                    ?>


                    <?php if( get_field('feito_com', 'option') ): ?>
                     <a href="#" target="_blank" class="feito" title="Feito com Qualitare">
                        <div class="size">
                            
                        <img src="<?php the_field('feito_com', 'option'); ?>">
                        </div>
                    </a>
                    <?php endif; ?>
                </div>

                <div class="fix-contact">
                    <?php if( get_field('link_whatsapp', 'option') ): ?>
                    <a href="<?php the_field('link_whatsapp', 'option'); ?>" class="whats">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/whats.svg">
                    </a>
                    <?php endif; ?>

                    <?php if ( have_rows('redes_sociais', 'option') ): ?>
                        <ul class="social">
                            <?php while ( have_rows('redes_sociais', 'option') ) : the_row(); ?>
                                <li>
                                    <a href="<?php the_sub_field('link'); ?>" target="_blank" title="<?php the_sub_field('nome'); ?>">
                                        <?php the_sub_field('nome'); ?>
                                    </a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    <?php endif; ?>
                </div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->


<div class="modal">
    <div class="follow alert">
        <button class="close">
            <img src="<?php echo get_template_directory_uri(); ?>/images/close.svg">
        </button>
        <?php the_field('alerta', 'option'); ?>
        <div class="btns-wrap">
            
            <a class="btn" href="<?php echo get_home_url(); ?>/minha-conta/#cadastre-se"  title="Cadastre-se"> 
             Cadastre-se
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <b>Cadastre-se</b>
              <b>Cadastre-se</b>
              <b>Cadastre-se</b>
              <b>Cadastre-se</b>
            </a>

            <h4>ou</h4>

            <a class="btn" href="<?php echo get_home_url(); ?>/minha-conta"  title="Entrar"> 
             Entrar
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <b>Entrar</b>
              <b>Entrar</b>
              <b>Entrar</b>
              <b>Entrar</b>
            </a>
        </div>
    </div>



    <div class="cart alert">
        <button class="close">
            <img src="<?php echo get_template_directory_uri(); ?>/images/close.svg">
        </button>
        <?php the_field('alerta_cart', 'option'); ?>
        <div class="btns-wrap">
            
            <a class="btn" href="<?php echo get_home_url(); ?>/minha-conta/#cadastre-se"  title="Cadastre-se"> 
             Cadastre-se
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <b>Cadastre-se</b>
              <b>Cadastre-se</b>
              <b>Cadastre-se</b>
              <b>Cadastre-se</b>
            </a>

            <h4>ou</h4>

            <a class="btn" href="<?php echo get_home_url(); ?>/minha-conta"  title="Entrar"> 
             Entrar
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <b>Entrar</b>
              <b>Entrar</b>
              <b>Entrar</b>
              <b>Entrar</b>
            </a>
        </div>
    </div>






    <div class="dadosnota alert">
        <button class="close">
            <img src="<?php echo get_template_directory_uri(); ?>/images/close.svg">
        </button>

        <div class="scroll-txt">
        <?php the_field('texto_dados', 'option'); ?>
          
        </div>

    </div>






    <div class="termoscad alert">
        <button class="close">
            <img src="<?php echo get_template_directory_uri(); ?>/images/close.svg">
        </button>

        <div class="scroll-txt">
        <?php the_field('texto_termos', 'option'); ?>
          
        </div>

    </div>
</div>

<?php wp_footer(); ?>

<script type="text/javascript">

jQuery(function(){
    var hasBeenTrigged = false;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() >= 50 && !hasBeenTrigged) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
             jQuery('header').addClass('scroll');
            hasBeenTrigged = true;
        } else if(jQuery(this).scrollTop() < 50 && hasBeenTrigged){
             jQuery('header').removeClass('scroll');
            hasBeenTrigged = false;

        }
    });



  setTimeout( function() {



  jQuery( ".woocommerce form .form-row input" ).each(function( index ) {
    if(jQuery(this).val()){
          jQuery(this).parent('.form-row').addClass('active');
          jQuery(this).parent().parent('.form-row').addClass('active');
      }
  });

  }, 500);


  jQuery( "input#tipo_pessoa_fisica" ).prop( "checked", true );



  jQuery('.cli-bar-btn_container a').attr('class', 'btn');


  jQuery('.cli-bar-btn_container a').html('Aceitar<span></span><span></span><span></span><span></span><b>Aceitar</b><b>Aceitar</b><b>Aceitar</b><b>Aceitar</b>');
  

  jQuery('.woocommerce-info .button').attr('class', 'btn-small');


});

jQuery( ".woocommerce form .form-row input" ).change(function() {
    if(jQuery(this).val()){
        jQuery(this).parent('.form-row').addClass('active');
        jQuery(this).parent().parent('.form-row').addClass('active');
    } else{
        jQuery(this).parent('.form-row').removeClass('active');
        jQuery(this).parent().parent('.form-row').removeClass('active');

    }
}); 


jQuery( ".woocommerce form .form-row input" ).focus(function() {
        jQuery(this).parent('.form-row').addClass('active');
        jQuery(this).parent().parent('.form-row').addClass('active');
});
jQuery( ".woocommerce form .form-row input" ).blur(function() {
    if(jQuery(this).val()){
        jQuery(this).parent('.form-row').addClass('active');
        jQuery(this).parent().parent('.form-row').addClass('active');
    } else{
        jQuery(this).parent('.form-row').removeClass('active');
        jQuery(this).parent().parent('.form-row').removeClass('active');

    }
});

jQuery('.wpb_video_widget.modal-video > .wpb_wrapper').append('<button class="close"><img src="<?php echo get_template_directory_uri(); ?>/images/close2.svg"></button>');
        


jQuery(document).delegate('a.btn.play-video', 'click', function(event) {
    event.preventDefault();

    var link = jQuery(this).attr('href');


    jQuery('.wpb_video_widget.modal-video' + link).addClass('active');
    jQuery('.wpb_video_widget.modal-video' + link + ' > .wpb_wrapper').addClass('active');
});


jQuery(document).delegate('.wpb_video_widget.modal-video button.close', 'click', function(event) {
    event.preventDefault();
    jQuery('.wpb_video_widget.modal-video').removeClass('active');
    jQuery('.wpb_video_widget.modal-video > .wpb_wrapper').removeClass('active');
});




jQuery(document).delegate('body.loggedout .yith-wcwl-add-to-wishlist a', 'click', function(event) {
    event.preventDefault();
    jQuery('.modal').addClass('active');
    jQuery('.modal .follow').addClass('active');
});



jQuery(document).delegate('.modal button.close', 'click', function(event) {
    event.preventDefault();
    jQuery('.modal').removeClass('active');
    jQuery('.modal .follow').removeClass('active');
});




jQuery('fieldset#pagarme-credit-cart-form .form-row').addClass('active');

/*
jQuery('form.woocommerce-form-login button[type="submit"]').addClass('btn');
jQuery('form.woocommerce-form-login button[type="submit"]').removeClass('button');
jQuery('form.woocommerce-form-login button[type="submit"]').html('Entrar<span></span><span></span><span></span><span></span><b>Entrar</b><b>Entrar</b><b>Entrar</b><b>Entrar</b>');
*/










//CARRINHO
jQuery('.page-carrinho .vc_btn3-container > *').prepend('<svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.28125 0.78125L0.78125 9.28125L0.09375 10L0.78125 10.7188L9.28125 19.2188L10.7188 17.7812L3.9375 11H24V9H3.9375L10.7188 2.21875L9.28125 0.78125Z" fill="#4A254B"/></svg>')




jQuery('form.checkout_coupon.woocommerce-form-coupon').insertAfter(jQuery('div#order_review table.shop_table.woocommerce-checkout-review-order-table'));

jQuery('form.checkout_coupon.woocommerce-form-coupon button').attr('class', 'btn-small');



if (jQuery(window).width() < 768) {
  jQuery('header nav.main-navigation .menu-principal-container ul.menu').append(jQuery('header ul.rt li.user-area'));



}



///checkout


jQuery(document).ready(function() {
  jQuery("#cpf").mask("999.999.999-99");
  jQuery("#cnpj").mask("99.999.999/9999-99");
  jQuery(".nasc_participante").mask("99/99/9999");


   jQuery(".telefone_part").mask("(99) 9999-9999?9");
  
  jQuery("#billing_postcode").mask("99999-999",{completed:function(){
    var cep = jQuery(this).val().replace(/[^0-9]/, "");
    
    // Validação do CEP; caso o CEP não possua 8 números, então cancela
    // a consulta
    if(cep.length != 8){
      return false;
    }
    
    // A url de pesquisa consiste no endereço do webservice + o cep que
    // o usuário informou + o tipo de retorno desejado (entre "json",
    // "jsonp", "xml", "piped" ou "querty")
    var url = "https://viacep.com.br/ws/"+cep+"/json/";
    
    jQuery.getJSON(url, function(dadosRetorno){
      try{
        // Preenche os campos de acordo com o retorno da pesquisa
        jQuery("#billing_address_1").val(dadosRetorno.logradouro);
        jQuery("#bairro").val(dadosRetorno.bairro);
        jQuery("#billing_city").val(dadosRetorno.localidade);
        jQuery("#estado").val(dadosRetorno.uf);
        jQuery("#num").focus();


          jQuery( ".woocommerce form .form-row input" ).each(function( index ) {
            if(jQuery(this).val()){
                  jQuery(this).parent('.form-row').addClass('active');
                  jQuery(this).parent().parent('.form-row').addClass('active');
              }
          });
      }catch(ex){}
    });



  }});
  
});



jQuery(document).delegate('.modal-nota', 'click', function(event) {
    event.preventDefault();
    jQuery('.modal').addClass('active');
    jQuery('.modal .dadosnota').addClass('active');
});




jQuery(document).delegate('.modal-termos', 'click', function(event) {
    event.preventDefault();
    jQuery('.modal').addClass('active');
    jQuery('.modal .termoscad').addClass('active');
});




  setTimeout( function() {
        
    jQuery( ".ywsl-social" ).each(function() {
      var nome = jQuery(this).attr('data-social');
      jQuery(this).html(nome);
      console.log(nome);
    });        

  }, 400);



</script>



<?php if ( is_front_page() ) : ?>


<script type="text/javascript">

jQuery(function(){

     setTimeout( function() {
        console.log(jQuery("#big-K").height());
        console.log(jQuery("#animated_anne_k").height());


        var controller = new ScrollMagic.Controller();



       if (jQuery(window).width() > 767) {

        // build scene
        var scene = new ScrollMagic.Scene({
            //triggerElement: "#trig", 
            //triggerHook: .94, 

            duration: ( jQuery("#big-K").height() - jQuery("#animated_anne_k").height() + 60 ), 
            offset: 0 // start this scene after scrolling for 50px
        })
        .setPin("#pinK") 
        .addTo(controller);

        
        // build scene
        var scene = new ScrollMagic.Scene({
            triggerElement: ".trigger1", 
            triggerHook: .2, 
            duration: ( jQuery("#cursos").height() - jQuery(".pinheight").height() ), 
            offset: 0 // start this scene after scrolling for 50px
        })
        .setPin("#pin-cursos") 
        .addTo(controller);


        

        // build scene
        var scene = new ScrollMagic.Scene({
            triggerElement: ".trigger2", 
            triggerHook: .2, 
            duration: ( jQuery("#mentorias").height() - jQuery(".pinheight2").height() ), 
            offset: 0 // start this scene after scrolling for 50px
        })
        .setPin("#pin-mentorias") 
        .addTo(controller);

      } 


        // build scene
        var scene = new ScrollMagic.Scene({
            triggerElement: "#hand1", 
            triggerHook: 1, 

            duration: ( jQuery("#hand1").height()*2.5 ), 
            offset: 0 // start this scene after scrolling for 50px
        })
        .setTween("#pin_hand1 figure", {transform: "translate(15%, -50%)"}) // trigger a TweenMax.to tween
        //.setPin("#pin_hand1") 
        .addTo(controller);











    }, 300);

});


    
    var theWindow = $(window);
    var winHeight = theWindow.height();
    //var animDuration = winHeight / 2;
    var animDuration = 2500;
    //console.log(animDuration);
    var animData = {
            container: document.getElementById('animated_anne_k'),
            renderer: 'canvas',
            loop: false,
            autoplay: false,
            path: '<?php echo get_template_directory_uri(); ?>/js/data_final.json'
        };
    var anim = bodymovin.loadAnimation(animData);

    jQuery(window).scroll(function() {
        animatebodymovin(animDuration, anim);
    });

    function animatebodymovin(duration, animObject) {
        
        var scrollPosition = theWindow.scrollTop();
        var maxFrames = animObject.totalFrames;
        var frame = (maxFrames / 100) * (scrollPosition / (duration / 100));
        
        animObject.goToAndStop(frame, true);
        
    }









</script>

<?php endif; ?>
</body>
</html>
