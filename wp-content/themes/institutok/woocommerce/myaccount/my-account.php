<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
do_action( 'woocommerce_account_navigation' ); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
<div class="woocommerce-MyAccount-content">
	<?php
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */
		do_action( 'woocommerce_account_content' );
	?>



<div id="cert" style="display: none;">
	<h2>Certificado</h2>
    <h3>Sample h3 tag</h3>
    <p>Sample pararaph</p>
    <img src="https://institutok.cc/wp-content/uploads/2020/02/modelo_certificado-scaled.jpg">
</div>
<button id="cmd"  style="display: none;">Generate PDF</button>
</div>
<script type="text/javascript">
jQuery(function(){
	jQuery('a.woocommerce-MyAccount-downloads-file.button.alt').attr('class', 'btn-small');
	jQuery('th.download-product span').html('Cursos');





});

jQuery(document).ready(function(){

	
	var st = jQuery('.order-status').html()
	if (st == "Concluído") {


	  	jQuery( ".woocommerce-table__product-name.product-name" ).each(function( index ) {
			var txt = jQuery(this).attr('texto-certificado');
			var nomecurso = jQuery(this).find('a').html();
			var pessoa = jQuery(this).attr('pessoa');
			var id = jQuery(this).attr('id');

			jQuery(this).append('<button class="btn-small certificado">Baixar certificado</button>')




				jQuery(this).find('.certificado').click(function () {   
				    doc.fromHTML(jQuery('.samp-cert').html(), 15, 15, {
        				'width': 170,
				            'elementHandlers': specialElementHandlers
				    });
				    doc.save('sample-file.pdf');
				});




		});
	  };


});
 


var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;

    }
};


 
</script>