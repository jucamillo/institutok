<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package institutok
 */

get_header();
?>


		<?php
		while ( have_posts() ) :
			the_post(); 


			if ( !is_front_page() ) :
			?>

				<style type="text/css">
				header .container .col-xs-12{
					border-color:transparent;
				}
				header{
					background: transparent;
				}
				#nome{
					background-color: <?php echo get_field('cor_acento'); ?>;

				}
				header.scroll,
				body{
					background-color:  <?php echo get_field('cor_background'); ?>;
				}
				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline,
				.woocommerce form .form-row input,
				header.scroll .container .col-xs-12{
					border-color: <?php echo get_field('cor_header'); ?>;
				}
				header nav.main-navigation .menu-toggle:after, header nav.main-navigation .menu-toggle:before, header nav.main-navigation .menu-toggle i{
					background-color: <?php echo get_field('cor_header'); ?>;

				}
				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline,
				footer .container,
				h2, h3, h4, h5, h1, body,
				header .container{
					color: <?php echo get_field('cor_header'); ?>;
				}
				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline svg *,
				footer svg *,
				header svg *,
				header .branding svg *{
					fill: <?php echo get_field('cor_header'); ?>;
				}
				.miolo-site .woocommerce table.shop_table thead th, .woocommerce .miolo-site table.shop_table thead th,
				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline:hover,
				header ul a.btn_header{
					background-color: <?php echo get_field('cor_header'); ?>;
					color: <?php echo get_field('cor_acento'); ?>;
				}

				body .miolo-site .vc_section .btn-sm-wp .vc_btn3.vc_btn3-style-outline:hover svg *{
					fill: <?php echo get_field('cor_acento'); ?>;

				}
				
				body.page-minha-conta.register-on header.scroll,
				body.page-minha-conta.register-on{
					background-color: <?php echo get_field('cor_acento'); ?>;

				}
				
				.woocommerce ul.order_details li{
					border-color: <?php echo get_field('cor_header'); ?>;
				}



				.inputs button:hover{
					color: <?php echo get_field('cor_acento'); ?> !important;
					background-color: <?php echo get_field('cor_header'); ?> !important;
				}
				.inputs input,
				.inputs,
				.inputs button{
					border-color: <?php echo get_field('cor_header'); ?> !important;
					color: <?php echo get_field('cor_header'); ?> !important;
				}



				.page-finalizar-compra .btn,
				.page-finalizar-compra a.btn,
				.page-finalizar-compra .alert a.btn,
				.page-carrinho a.btn,
				.page-carrinho .alert a.btn{

				  --buttonColor: <?php echo get_field('cor_header'); ?>;
				  --hoverColor: #D7D8D8;
				  --buttonShadowColor: rgba(74,37,75,.5);
				}
				.page-finalizar-compra .modal .alert button.close svg *,
				.page-carrinho .modal .alert button.close svg *{
					fill: <?php echo get_field('cor_header'); ?>;

				}
				.page-finalizar-compra a.btn b svg *,
				.page-carrinho a.btn b svg *{
					fill: <?php echo get_field('cor_acento'); ?>;
				}

				.page-finalizar-compra .modal .alert,
				.page-carrinho .modal .alert{
					background: <?php echo get_field('cor_background'); ?>;
				}

				ul.thwmscf-tabs li a.active{
					color:#D7D8D8;
					background: <?php echo get_field('cor_header'); ?>;
				}


				</style>
			<?php
			endif;

			get_template_part( 'template-parts/content', 'page' );

		endwhile; // End of the loop.
		?>

<?php
get_footer();
