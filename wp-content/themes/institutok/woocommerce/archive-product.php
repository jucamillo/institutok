<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */

?>
<section class="miolo-produto">
	<div class="container">
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<?php echo do_shortcode('[searchandfilter slug="filtro-produtos"]'); ?>

			
		</div>
		<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12" id="list-results">
			<?php //echo do_shortcode('[products search_filter_id="139"]'); ?>
			<?php
				if ( woocommerce_product_loop() ) {

					/**
					 * Hook: woocommerce_before_shop_loop.
					 *
					 * @hooked woocommerce_output_all_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );

					woocommerce_product_loop_start();

					if ( wc_get_loop_prop( 'total' ) ) {
						while ( have_posts() ) {
							the_post();

							/**
							 * Hook: woocommerce_shop_loop.
							 */
							do_action( 'woocommerce_shop_loop' );

							wc_get_template_part( 'content', 'product' );
						}
					}

					woocommerce_product_loop_end();

					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					//do_action( 'woocommerce_after_shop_loop' );
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				}

				/**
				 * Hook: woocommerce_after_main_content.
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action( 'woocommerce_after_main_content' ); 
				?>
		</div>
	</div>
</section>
<script type="text/javascript">

jQuery(document).delegate('.searchandfilter ul li li label', 'click', function(event) {


	setTimeout( function() {
		jQuery('.searchandfilter ul li li input[value="seguindo"]').attr("disabled", true);
	}, 1000); 


});
jQuery(document).delegate('body.loggedout  .searchandfilter ul li li input[value="seguindo"] + label', 'click', function(event) {


	
	jQuery('.searchandfilter ul li li input').prop( "checked", false );
    jQuery('.modal').addClass('active');
    jQuery('.modal .follow').addClass('active');


});

jQuery(document).delegate('body.loggedin  .searchandfilter ul li li input[value="seguindo"] + label', 'click', function(event) {
    event.preventDefault();
    window.location.href = '<?php echo get_home_url(); ?>/cursos/seguindo';
});

jQuery(document).delegate('.searchandfilter ul li li input + label', 'click', function(event) {
    //event.preventDefault();
	setTimeout( function() {
		console.log('filtrou');
			//jQuery('.searchandfilter ul li li input[value="seguindo"]').attr("disabled", true);
			jQuery( "ul li.product" ).each(function() {
				
			    var id = jQuery(this).find('.link-curso').attr('id');

				jQuery(this).find('.yith-wcwl-add-to-wishlist').html('<div class="yith-wcwl-add-button"><a href="https://homolog.jucamillo.com.br/institutok/cursos?post_type=product&amp;add_to_wishlist=' + id + '" rel="nofollow" data-product-id="' + id + '" data-product-type="simple" data-original-product-id="' + id + '" class="add_to_wishlist single_add_to_wishlist" data-title="SEGUIR ESTE CURSO"><i class="yith-wcwl-icon fa fa-heart-o"></i><span>SEGUIR ESTE CURSO</span></a></div>')
			  jQuery( this ).addClass( "foo" );
			});




	}, 2000); 

});




jQuery(function(){

	setTimeout( function() {

			jQuery('.searchandfilter ul li li input[value="seguindo"]').attr("disabled", true);
	}, 400); 
});

</script>
<?php

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
//do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
