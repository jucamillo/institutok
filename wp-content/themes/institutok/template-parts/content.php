<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package institutok
 */

    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

    if ( !is_singular() ) :
    	echo '<div class="post-latest">
    			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
	                <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="img" style="background-image:url('.$image[0].');">

	                </a>
                </div>
    			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <ul class="blog-categories">';

                    $categories = wp_get_post_categories( get_the_ID() );
                        //loop through them
                        foreach($categories as $c){
                          $cat = get_category( $c );
                          //get the name of the category
                          $cat_id = get_cat_ID( $cat->name );
                          //make a list item containing a link to the category
                           echo '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></li>';
                        }
                    echo '</ul> 

                    <a href="'.get_the_permalink().'" title="'.get_the_title().'" class="txts">
                        <h4>'.get_the_title().'</h4>
                        <p>';

                        custom_length_excerpt(65);

                        echo '</p>
                    </a>  

                </div>  
                </div>';
            

            else : ?>
			<section class="single-blog">
				<div class="container">
					<div class="row">
						<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12">
							
						</div>
						<div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
							


						<h1><?php the_title(); ?></h1>


						<?php
						institutok_post_thumbnail();
						echo '<div class="content-info-noticias">';
						the_content( );
						echo '</div> ';

			 			?>
							<div class="compartilhe">
								<h3>Compartilhe</h3><?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
							</div>
							<div class="fb-comments" data-href="<?php echo get_permalink(); ?>" data-width="100%" data-numposts="8"></div>

							<div class="read">
								<h2>Leitores também acessaram:</h2>
				 				<div class="row"><?php echo do_shortcode( '[my_related_posts]' ); ?>	</div>
							</div>


						</div>
						<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12">
							
						</div>
					</div>
				</div>
			</section>
		<?php endif; ?>