<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package institutok
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />


	 <?php if (!is_page('carrinho')):?>


	 <?php if (!is_page('finalizar-compra')):?>
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>

	<?php endif; ?>
	<?php endif; ?>

	
	 <?php if ( is_front_page() ) : ?>
	 	<script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.5.9/lottie.js"></script>
	<?php endif; ?>
	
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>



	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js" ></script>


    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js" type="text/javascript"></script>


</head>

<body <?php body_class(); ?>>

<?php global $current_user; wp_get_current_user(); 
global $woocommerce;
?>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Pular para o conteúdo', 'institutok' ); ?></a>

	<header class="site-header">
		<div class="container">
			<div class="col-xs-12">
				<div class="lf">
						
					<div class="branding">
						<?php
						the_custom_logo();
						?>
					</div>


					<nav id="site-navigation" class="main-navigation">
						<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i></i></button>
						<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						) );
						?>

					</nav>
				</div>

				<ul class="rt">
					<li class="user-area">
						<?php if ( is_user_logged_in() ) { ?>

						<a href="<?php echo get_home_url(); ?>/minha-conta">
							<?php the_field('bem_vindo', 'option'); ?>
							<?php  echo $current_user->display_name ; ?> 
						</a>
						<ul>
							<li>
								<a href="<?php echo get_home_url(); ?>/minha-conta/cursos/" title="Cursos">Cursos</a>
							</li>
							<li>
								<a href="<?php echo get_home_url(); ?>/minha-conta/materiais/" title="Materiais">Materiais</a>
							</li>
							<li>
								<a href="<?php echo get_home_url(); ?>/minha-conta/dados-da-nota/" title="Dados da Nota">Dados da Nota</a>
							</li>
							<li>
								<a href="<?php echo get_home_url(); ?>/minha-conta/dados-da-conta/" title="Dados da Conta">Dados da Conta</a>
							</li>
							<li>
								<a href="<?php echo get_home_url(); ?>/minha-conta/sair/" title="Sair">Sair</a>
							</li>
						</ul>
						<?php } else { ?>

						<a href="<?php echo get_home_url(); ?>/minha-conta" title="<?php the_field('faca_login', 'option'); ?> ">
							<?php the_field('faca_login', 'option'); ?> 
						</a>

						<?php } ?>
					</li>
					<li>
						<a href="<?php echo get_home_url(); ?>/carrinho/" title="Carrinho" class="cart">
							<img src="<?php echo get_template_directory_uri(); ?>/images/cart.svg">
							<span>
								<?php echo $woocommerce->cart->cart_contents_count; ?>
							</span>
						</a>
					</li>
                    <?php if( get_field('link_botao', 'option') ): ?>
					<li>
						<a href="<?php the_field('link_botao', 'option'); ?>" class="btn_header">
                            <?php the_field('botao', 'option'); ?>
                        </a>
                    </li>
                    <?php endif; ?>
				</ul>
			</div>
		</div>
	</header><!-- #masthead -->


	<section id="content" class="miolo-site">